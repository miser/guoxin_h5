// process.env.NODE_ENV === 'production'

const webpack = require("webpack");

const publicPath = "/asset/industrychain/";

module.exports = {
  publicPath,
  devServer: {
    // port: 8080,
    disableHostCheck: true,
    port: 80,
    host: "localhost"
  },
  chainWebpack: config => {
    const oneOfsMap = config.module.rule("scss").oneOfs.store;
    oneOfsMap.forEach(item => {
      item
        .use("sass-resources-loader")
        .loader("sass-resources-loader")
        .options({
          resources: "./src/assets/scss/base.scss"
        })
        .end();
    });

    config.plugin("DefinePlugin").use(webpack.DefinePlugin, [
      {
        "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV || "development"),
        IS_PROD: process.env.NODE_ENV === "production" ? true : false,
        // IS_PROD: true,
        PUBLIC_PATH: `"${publicPath}"`
      }
    ]);
  }
};

// /asset/industrychain/
