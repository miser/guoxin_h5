import Vue from "vue";
import Vuex from "vuex";
import search from "./modules/search";
import user from "./modules/user";
import company from "./modules/company";
import product from "./modules/product";
import createLogger from "../plugins/logger";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
  modules: {
    user,
    search,
    company,
    product
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
});

// Com.newWebViewOpenPage("", { pageUrl: url, weburl: url });
// TalkingData && TalkingData.onEventWithLabel('行情-产业图谱', '首页打开次数');
// TalkingData && TalkingData.onEventWithLabel('行情-产业图谱', '一图看懂产业链图谱按钮的点击次数');   第二个就这样吗?只要自己定义好对方，告知大家，后台能查到数据就行是吗？
