import request from "../../utils/request";

// initial state
const originState = {
  company: {},
  related: null
};

// getters
const getters = {};

// actions
const actions = {
  get({ commit }, opts) {
    request.post("/company/queryCompanyBaseInfo", opts).then(res => {
      commit("setCompany", res);
    });
  },
  getRelated({ commit }, opts) {
    request
      .get("/company/queryRelatedCompanys", opts)
      .then(res => {
        commit("setRelated", res);
      })
      .catch(() => {
        commit("setRelated", []);
      });
  }
};

// mutations
const mutations = {
  // // search
  setCompany(state, company) {
    state.company = company;
  },
  // Related
  setRelated(state, list) {
    state.related = list;
  },
  clearCompany(state) {
    state.company = {};
  },
  clearRelated(state) {
    state.related = null;
  }
};

export default {
  namespaced: true,
  state: originState,
  getters,
  actions,
  mutations
};
