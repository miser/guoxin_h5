import request from "../../utils/request";

// initial state
const originState = {
  businessProducts: null,
  chainProducts: null,
  chainList: {},
  hotList: null
};

// getters
const getters = {
  hot: state => {
    const hotList = state.hotList;
    if (!hotList) return null;
    return hotList.map(item => ({
      productCode: item.productCode,
      name:
        item.productName.length <= 5 ? item.productName : `${item.productName.substring(0, 5)}...`,
      productName: item.productName
    }));
  }
};

// actions
const actions = {
  getHotList({ commit }, opts) {
    request
      .get("/product/queryHotChain", opts)
      .then(res => {
        commit("setHotList", res);
      })
      .catch(() => {
        commit("setHotList", []);
      });
  },
  getBusinessProducts({ commit }, opts) {
    request
      .get("/product/queryMainBusinessProducts", opts)
      .then(res => {
        commit("setBusinessProducts", res);
      })
      .catch(() => {
        commit("setBusinessProducts", []);
      });
  },
  getChainProducts({ commit }, opts) {
    request
      .get("/product/queryChainProducts", opts)
      .then(res => {
        commit("setChainProducts", res);
      })
      .catch(() => {
        commit("setChainProducts", []);
      });
  },
  getUpAndDownChain({ commit }, opts) {
    request.get("/product/queryUpAndDownChain", opts).then(res => {
      commit("setUpAndDownChain", { productCode: opts.productCode, ...res });
    });
  }
};

// mutations
const mutations = {
  setHotList(state, list) {
    state.hotList = list;
  },
  setBusinessProducts(state, product) {
    state.businessProducts = product;
  },
  setChainProducts(state, product) {
    state.chainProducts = product;
  },
  setUpAndDownChain(state, res) {
    state.chainList[res.productCode] = res;
  },
  // clear

  clearBusinessProducts(state) {
    state.businessProducts = [];
  },
  clearChainProducts(state) {
    state.chainProducts = null;
  },
  clearChainList(state) {
    state.chainList = {};
  },
  clearHotList(state) {
    state.hotList = null;
  }
};

export default {
  namespaced: true,
  state: originState,
  getters,
  actions,
  mutations
};
