import { getBridge } from "@/utils";
import request from "../../utils/request";

const userState = {
  user: null,
  userStock: null
};

const getters = {};

// actions
const actions = {
  getUser({ commit }) {
    const bridge = getBridge();
    if (bridge) {
      bridge.getCommonLoginData(user => {
        console.log(`getCommonLoginData callback: %j`, user);
        commit("setUser", { ...user, userId: user.userCode });
      });
    }
  },
  getUserStock({ commit }, { userId }) {
    const bridge = getBridge();
    if (bridge) {
      bridge.getSelectedStock(list => {
        const i1 = list.selectedStock.indexOf("000001.2");
        if (i1 >= 0) {
          list.selectedStock.splice(i1, 1);
        }
        const i2 = list.selectedStock.indexOf("000001.SH");
        if (i2 >= 0) {
          list.selectedStock.splice(i2, 1);
        }

        const opts = {
          userId,
          codeListJson: JSON.stringify(list.selectedStock.map(code => code.split(".")[0]))
        };

        if (opts.codeListJson === "[]") {
          commit("setUserStock", []);
        } else {
          request
            .post("/company/queryMyOptionalStock", opts)
            .then(res => {
              commit("setUserStock", res);
            })
            .catch(() => {
              commit("setUserStock", []);
            });
        }
      });
    }
  }
};

// mutations
const mutations = {
  setUser(state, user) {
    state.user = user;
  },
  setUserStock(state, list) {
    state.userStock = list;
  },
  clearUserStock(state) {
    state.userStock = null;
  }
};

export default {
  namespaced: true,
  state: userState,
  getters,
  actions,
  mutations
};
