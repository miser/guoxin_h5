import request from "../../utils/request";

// initial state
const originState = {
  list: [],
  history: [],
  isSearching: false
};

// getters
const getters = {
  industry: state => {
    const productChain = state.list.productChain || [];
    return productChain.slice(0, 20);
  },
  company: state => {
    const company = state.list.company || [];
    return company.slice(0, 20);
  }
};

// actions
const actions = {
  // search
  search({ commit }, opts) {
    commit("openSearching", true);
    request
      .get("/search", opts)
      .then(res => {
        commit("setSearchList", res);
      })
      .catch(() => {
        commit("cancelSearching", true);
      });
  },
  // history
  addHistory(_, opts) {
    request.post("/addSearchHistory", opts);
  },
  getHistory({ commit }, opts) {
    request.get("/querySearchHistory", opts).then(res => {
      commit("setHistory", res);
    });
  },
  clearHistory(_, opts) {
    request.get("/dropSearchHistory", opts);
  }
};

// mutations
const mutations = {
  // search
  openSearching(state) {
    state.isSearching = true;
  },
  cancelSearching(state) {
    state.isSearching = false;
  },
  clearSearchList(state) {
    state.list = [];
  },
  setSearchList(state, list) {
    state.list = list;
    this.cancelSearching(state);
  },
  // history
  setHistory(state, list) {
    state.history = list;
  },
  clearHistory(state) {
    state.history = [];
  }
};

export default {
  namespaced: true,
  state: originState,
  getters,
  actions,
  mutations
};
