export function computeVal(standardVal, curWidth, designWidth) {
  return Math.floor((standardVal * curWidth) / designWidth);
}

export function trans2REMNum(value, baseValue = 75) {
  return +(value / baseValue).toFixed(2);
}

export function trans2REM(value, baseValue = 75) {
  return `${trans2REMNum(value, baseValue)}rem`;
}
