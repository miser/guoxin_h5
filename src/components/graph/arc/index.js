import * as d3 from "d3";
import { trans2REM, computeVal } from "../../utils";
import colors from "./color";

class Arc {
  constructor(el, title, data, clickFn, isThemeDark) {
    this.isThemeDark = isThemeDark;
    this.title = title;
    this.clickFn = clickFn;
    this.data = data;
    this.svg = d3
      .select(el)
      .append("svg")
      .attr("width", "100%");
    // .attr("height", computeVal(332));

    const n = this.svg.node();
    this.width = n.getBoundingClientRect().width;
    this.computeVal = val => computeVal(val, this.width, 690);

    this.svg.attr("height", this.computeVal(332));
    this.colors = colors;
  }

  createCircle(index) {
    const cX = this.computeVal(325);
    const cY = (index + 1) * this.computeVal(70);

    const c = d3
      .create("svg:circle")
      .attr("cx", cX)
      .attr("cy", cY)
      .attr("r", this.computeVal(10))
      .attr("fill", this.colors[index]);

    return c;
  }

  createFont(txt, index) {
    const fontSize = 26;
    const x = this.computeVal(325 + 26);
    const y = (index + 1) * this.computeVal(70) + this.computeVal(7);

    const c = d3
      .create("svg:text")
      .style("font-size", trans2REM(fontSize))
      .attr("x", x)
      .attr("y", y)
      .attr("fill", this.isThemeDark ? "#D6D8E2" : "#212634")
      .attr("font-weight", "normal")
      .text(txt);
    return c;
  }

  createFont2(txt, index) {
    const fontSize = 24;
    const y = (index + 1) * this.computeVal(70) + this.computeVal(7);

    const c = d3
      .create("svg:text")
      .style("font-size", trans2REM(fontSize))
      .attr("x", "95%")
      .attr("y", y)
      .attr("text-anchor", "end")
      .attr("fill", this.isThemeDark ? "#D6D8E2" : "#212634")
      .text(txt);

    return c;
  }

  createName(originTxt) {
    originTxt = originTxt.split(" ").join("");

    const txt = originTxt.length <= 4 ? originTxt : `${originTxt.substring(0, 3)}...`;
    const nameEl = d3
      .create("svg:text")
      .style("font-size", trans2REM(26))
      .attr("fill", this.isThemeDark ? "#D6D8E2" : "#212634");

    // text-anchor="middle"
    nameEl
      .append("tspan")
      .attr("text-anchor", "middle")
      .text(txt.substring(0, 2));

    nameEl
      .append("tspan")
      .attr("text-anchor", "middle")
      .text(txt.substring(2));

    return nameEl;
  }

  render() {
    setTimeout(() => {
      const svgNode = this.svg.node();
      const width = svgNode.width.baseVal.value;
      const height = svgNode.height.baseVal.value;

      const innerR = width / 10;
      const outerR = width / 6;
      const tX = width / 20 + outerR;
      const tY = height / 6 + outerR;

      this.arc = d3
        .arc()
        .innerRadius(innerR)
        .outerRadius(outerR);
      const arc = this.arc;

      const svg = this.svg;
      const g = svg.append("g").attr("transform", `translate(${tX},${tY})`);

      const data = this.data.map(item => item.ratio);
      const arcs = d3.pie()(data);

      g.selectAll("path")
        .data(arcs)
        .enter()
        .append("path")
        .style("fill", (d, i) => this.colors[i])
        .on("click", this.clickFn)
        .attr("d", arc);

      const nameTxt = g.insert(() => this.createName(this.title).node());
      // console.log(nameTxt.selectAll("tspan"));
      nameTxt.selectAll("tspan")._groups[0].forEach((element, index) => {
        const eBox = element.getBBox();

        d3.select(element)
          .attr("x", 0)
          .attr("y", index * eBox.height + eBox.y / 4);
      });
      // console.log(nameTxt);

      // console.log(tspan1.node().getBBox());

      // const tspan1El = tspan1._groups[0][0];

      // const tBox = tspan1El.getBBox();
      // d3.select(t)
      //   .attr("x", x + (width - tBox.width) / 2)
      //   .attr("y", y + (height - tBox.height) / 2 + this.computeVal(30));

      // const x = element.children[0].x.baseVal.value;
      // const y = element.children[0].y.baseVal.value;
      // const width = element.children[0].width.baseVal.value;
      // const height = element.children[0].height.baseVal.value;

      const txtG = this.svg.append("g");

      for (let i = 0; i < this.data.length; i++) {
        const d = this.data[i];
        const tg = txtG.append("g");
        let name = d.productName;
        if (name.length > 7) {
          name = `${name.substring(0, 7)}...`;
        }
        tg.insert(() => this.createCircle(i, width).node());
        tg.insert(() => this.createFont(name, i).node());
        tg.insert(() => this.createFont2(`${d.ratio}%`, i).node());
      }
    });
  }
}

export default Arc;
