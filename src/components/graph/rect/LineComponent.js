import * as d3 from "d3";

class LineComponent {
  static create(pClass, color) {
    const p = d3
      .create("svg:path")
      .attr("class", pClass)
      .attr("fill", "white")
      .attr("fill-opacity", "0")
      .attr("stroke", color)
      .attr("stroke-width", "1")
      .attr("stroke-dasharray", "3");

    return p;
  }
}

export default LineComponent;
