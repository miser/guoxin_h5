class RectComponent {
  constructor(graph) {
    this.graph = graph;
  }

  computeY(startMargin, index, height, marginTop) {
    const computeVal = this.graph.computeVal;

    return computeVal(startMargin) + computeVal(height + marginTop) * index;
  }
}

export default RectComponent;
