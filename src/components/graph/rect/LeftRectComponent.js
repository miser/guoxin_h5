import SubRectComponent from "./SubRectComponent";

const X = 53;

class LeftRectComponent extends SubRectComponent {
  create(index, color, cb) {
    return super.create(index, X, "g-left-rect", color, cb);
  }
}

export default LeftRectComponent;
