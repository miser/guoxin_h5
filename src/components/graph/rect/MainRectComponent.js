import * as d3 from "d3";
import RectComponent from "./RectComponent";
import CircleComponent from "./CircleComponent";

class MainRectComponent extends RectComponent {
  // constructor(graph) {
  //   super();
  //   this.graph = graph;
  // }

  create(index, color, cb) {
    const h = 70;
    const y = this.computeY(113, index, h, 34);

    const computeVal = this.graph.computeVal;

    const rect = d3
      .create("svg:rect")
      .attr("class", "m-rect")
      .attr("x", computeVal(267))
      .attr("y", y)
      .attr("rx", 6)
      .attr("ry", 6)
      .attr("width", computeVal(168))
      .attr("height", computeVal(h))
      // .attr("stroke", color)
      .attr("fill", color)
      // .attr("stroke-width", "2")
      .on("click", cb);

    const lCircle = d3
      .create("svg:circle")
      .attr("class", "m-l-circle")
      .attr("cx", computeVal(267))
      .attr("cy", computeVal(CircleComponent.computeY(113, index, h, 34)))
      .attr("r", "0");

    const rCircle = d3
      .create("svg:circle")
      .attr("class", "m-r-circle")
      .attr("cx", computeVal(267 + 168))
      .attr("cy", computeVal(CircleComponent.computeY(113, index, h, 34)))
      .attr("r", "0");

    const g = d3.create("svg:g").attr("class", "g-m-rect");
    g.append(() => rect.node());
    g.append(() => lCircle.node());
    g.append(() => rCircle.node());

    return g;
  }
}

export default MainRectComponent;
