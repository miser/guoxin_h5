import * as d3 from "d3";

class CircleComponent {
  constructor(graph) {
    this.graph = graph;
  }

  _create(index, x, n, color) {
    const computeVal = this.graph.computeVal;
    let y;
    if (n) {
      const b = n.getBBox();
      y = b.y + b.height / 2;
    } else {
      y = computeVal(CircleComponent.computeY(127, index, 50, 34));
    }

    const c = d3
      .create("svg:circle")
      .attr("cx", computeVal(x))
      .attr("cy", y)
      .attr("r", "3")
      .attr("fill", color);

    return c;
  }

  static computeY(startMargin, index, height, marginTop) {
    return startMargin + (height + marginTop) * index + height / 2;
  }
}

export default CircleComponent;
