import CircleComponent from "./CircleComponent";

class RightCircleComponent extends CircleComponent {
  create(index, n, color) {
    return this._create(index, 499, n, color);
  }
}

export default RightCircleComponent;
