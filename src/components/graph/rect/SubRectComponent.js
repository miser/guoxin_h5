import * as d3 from "d3";
import RectComponent from "./RectComponent";

class SubRectComponent extends RectComponent {
  create(index, x, gClass, color, cb) {
    const y = this.computeY(123, index, 50, 34);
    const computeVal = this.graph.computeVal;

    const rect = d3
      .create("svg:rect")
      .attr("x", computeVal(x))
      .attr("y", y)
      .attr("rx", 6)
      .attr("ry", 6)
      .attr("width", computeVal(140))
      .attr("height", computeVal(50))
      .attr("stroke", color)
      .attr("fill-opacity", 0)
      // .attr("fill", "white")
      .attr("stroke-width", "1")
      .on("click", cb);

    const g = d3.create("svg:g").attr("class", `g-small-rect ${gClass}`);

    g.append(() => rect.node());

    return g;
  }
}

export default SubRectComponent;
