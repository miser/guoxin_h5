import LeftRectComponent from "./LeftRectComponent";
import RightRectComponent from "./RightRectComponent";
import MainRectComponent from "./MainRectComponent";
import LeftCircleComponent from "./LeftCircleComponent";
import RightCircleComponent from "./RightCircleComponent";
import LineComponent from "./LineComponent";

export {
  LeftRectComponent,
  RightRectComponent,
  MainRectComponent,
  LeftCircleComponent,
  RightCircleComponent,
  LineComponent
};
