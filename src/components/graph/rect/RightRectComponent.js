import SubRectComponent from "./SubRectComponent";

const X = 499;

class RightRectComponent extends SubRectComponent {
  create(index, color, cb) {
    return super.create(index, X, "g-right-rect", color, cb);
  }
}

export default RightRectComponent;
