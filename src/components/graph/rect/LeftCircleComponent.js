import CircleComponent from "./CircleComponent";

class LeftCircleComponent extends CircleComponent {
  create(index, n, color) {
    return this._create(index, 193.5, n, color);
  }
}

export default LeftCircleComponent;
