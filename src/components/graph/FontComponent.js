import * as d3 from "d3";
import { trans2REM } from "../utils";

function createFont(txt, fontSize, color = "#202533") {
  return d3
    .create("svg:text")
    .attr("fill", color)
    .style("font-size", trans2REM(fontSize))
    .text(txt);
}
class FontComponent {
  static create(txt, cb) {
    const el = createFont(txt, 24, "white");
    if (cb) {
      el.on("click", cb);
    }
    return el;
  }

  static createSubFont(txt, cb, color) {
    const el = createFont(txt, 24, color);
    if (cb) {
      el.on("click", cb);
    }
    return el;
  }
}

export default FontComponent;
