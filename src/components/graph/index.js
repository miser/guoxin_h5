import * as d3 from "d3";
import { computeVal } from "../utils";
import {
  LineComponent,
  RightRectComponent,
  LeftRectComponent,
  MainRectComponent,
  LeftCircleComponent,
  RightCircleComponent
} from "./rect";
import FontComponent from "./FontComponent";

function c(start, end) {
  const { x: sX, y: sY } = start;
  const { x: eX, y: eY } = end;

  const mX = (sX - eX) / 2 + eX;

  let p = "";
  p += `M${sX},${sY}`;
  p += `,L${mX},${sY}`;
  p += `,L${mX},${eY}`;
  p += `,L${eX},${eY}`;

  return p;
}

class Graph {
  constructor(el, mainData = [], fetchSubData, cb, subCb, isThemeDark) {
    this.isThemeDark = isThemeDark;
    this.mainData = mainData;
    this.fetchSubData = fetchSubData;
    this.cb = cb;
    this.subCb = subCb;

    this.leftData = [];
    this.rightData = [];

    this.subColor = null;

    this.svg = d3
      .select(el)
      .append("svg")
      .attr("width", "100%");
    // .attr("height", "700px");
    this.currMainRectIndex = null;

    // window.addEventListener("resize", () => {
    //   Graph.clear();
    //   // this.renderPath();
    // });
  }

  drawHeader() {
    const titleG = this.svg.append("g").classed("header", true);

    titleG
      .append("text")
      .attr("x", this.computeVal(95))
      .attr("y", this.computeVal(28 + 22))
      .attr("fill", this.isThemeDark ? "#D6D8E2" : "#212634")
      .classed("t1", true)
      .text("上游");

    titleG
      .append("text")
      .attr("x", this.computeVal(541))
      .attr("y", this.computeVal(28 + 22))
      .attr("fill", this.isThemeDark ? "#D6D8E2" : "#212634")
      .classed("t2", true)
      .text("下游");

    titleG
      .append("line")
      .attr("x1", this.computeVal(30))
      .attr("y1", this.computeVal(50 + 18))
      .attr("x2", this.computeVal(660))
      .attr("y2", this.computeVal(50 + 18))
      .style("stroke", this.isThemeDark ? "#0F1116" : "#EAEFFA")
      .style("stroke-width", computeVal(2));
  }

  drawLeft() {
    const g = this.svg.append("g").attr("class", "g-left");

    this.leftData.forEach((item, index) => {
      const lc = new LeftRectComponent(this);
      const lcc = new LeftCircleComponent(this);
      let n;
      g.insert(() => {
        n = lc.create(index, this.subColor, this.onClickSub.bind(this, item)).node();
        return n;
      });
      g.insert(() => lcc.create(index, n, this.subColor).node());
    });

    const leftRects = d3.selectAll(".g-left-rect");
    leftRects._groups[0].forEach((element, index) => {
      const x = element.children[0].x.baseVal.value;
      const y = element.children[0].y.baseVal.value;
      const width = element.children[0].width.baseVal.value;
      const height = element.children[0].height.baseVal.value;
      let name = this.leftData[index].productName;
      if (name.length > 5) {
        name = `${name.substring(0, 5)}...`;
      }
      d3.select(element).insert(() =>
        FontComponent.createSubFont(
          name,
          this.onClickSub.bind(this, this.leftData[index]),
          this.isThemeDark ? "#d6d8e2" : null
        ).node()
      );

      const t = d3.select(element).select("text")._groups[0][0];
      const tBox = t.getBBox();
      const padding = this.computeVal(16);

      d3.select(t)
        .attr("x", x + width - padding - tBox.width)
        .attr("y", y + (height - tBox.height) / 2 + this.computeVal(24));

      d3.select(element.children[0])
        .attr("x", x + width - padding * 2 - tBox.width)
        .attr("width", tBox.width + padding * 2);
    });
  }

  drawRight() {
    const g = this.svg.append("g").attr("class", "g-right");
    this.rightData.forEach((item, index) => {
      const rc = new RightRectComponent(this);
      const rcc = new RightCircleComponent(this);
      let n;
      g.insert(() => {
        n = rc.create(index, this.subColor, this.onClickSub.bind(this, item)).node();
        return n;
      });
      g.insert(() => rcc.create(index, n, this.subColor).node());
    });

    const rightRects = d3.selectAll(".g-right-rect");
    rightRects._groups[0].forEach((element, index) => {
      const x = element.children[0].x.baseVal.value;
      const y = element.children[0].y.baseVal.value;
      const width = element.children[0].width.baseVal.value;
      const height = element.children[0].height.baseVal.value;
      let name = this.rightData[index].productName;
      if (name.length > 5) {
        name = `${name.substring(0, 5)}...`;
      }
      d3.select(element).insert(() =>
        // d6d8e2
        FontComponent.createSubFont(
          name,
          this.onClickSub.bind(this, this.rightData[index]),
          this.isThemeDark ? "#d6d8e2" : null
        ).node()
      );

      const t = d3.select(element).select("text")._groups[0][0];
      // const tBox = t.getBBox();
      // d3.select(t)
      //   .attr("x", x + (width - tBox.width) / 2)
      //   .attr("y", y + (height - tBox.height) / 2 + this.computeVal(24));
      const tBox = t.getBBox();
      const padding = this.computeVal(16);
      d3.select(t)
        .attr("x", x + padding)
        .attr("y", y + (height - tBox.height) / 2 + this.computeVal(24));

      d3.select(element.children[0]).attr("width", tBox.width + padding * 2);
    });
  }

  async onClickMain(index) {
    this.currMainRectIndex = index;
    const item = this.mainData[index];

    if (!item) return;

    this.subColor = item.color;

    if (this.cb) {
      this.cb(item);
    }

    const subData = await this.fetchSubData(item.productCode);

    this.leftData = subData.up;
    this.rightData = subData.down;

    Graph.clear();

    this.drawLeft();
    this.drawRight();

    this.renderPath();

    // m-rect
    // g-right-rect
    // g-left-rect
    const mRects = d3.selectAll(".m-rect")._groups[0];
    const lastMReact = mRects[mRects.length - 1];
    const mB = lastMReact.getBBox();
    const mH = mB.y + mB.height;

    const rRects = d3.selectAll(".g-right-rect")._groups[0];
    const lastRReact = rRects[rRects.length - 1];
    let rH = 0;
    if (lastRReact) {
      const rB = lastRReact.getBBox();
      rH = rB.y + rB.height;
    }

    const lRects = d3.selectAll(".g-left-rect")._groups[0];
    const lastLReact = lRects[lRects.length - 1];
    let lH = 0;
    if (lastLReact) {
      const lB = lastLReact.getBBox();
      lH = lB.y + lB.height;
    }

    const maxH = Math.max(mH, rH, lH);
    this.svg.attr("height", maxH + this.computeVal(100));
  }

  onClickSub(item) {
    if (this.subCb) {
      this.subCb(item);
    }
  }

  drawMiddle() {
    const g = this.svg.append("g").attr("class", "g-middle");
    this.mainData.forEach((item, index) => {
      const mc = new MainRectComponent(this);
      g.insert(() => mc.create(index, item.color, this.onClickMain.bind(this, index)).node());
    });
  }

  static clear() {
    d3.selectAll(".g-left").remove();
    d3.selectAll(".g-right").remove();

    d3.selectAll(".l-left").remove();
    d3.selectAll(".l-right").remove();
  }

  renderFont() {
    const rects = d3.selectAll(".g-m-rect");

    rects._groups[0].forEach((element, index) => {
      const x = element.children[0].x.baseVal.value;
      const y = element.children[0].y.baseVal.value;
      const width = element.children[0].width.baseVal.value;
      const height = element.children[0].height.baseVal.value;
      let name = this.mainData[index].productName;
      if (name.length > 5) {
        name = `${name.substring(0, 5)}...`;
      }
      d3.select(element).insert(() =>
        FontComponent.create(name, this.onClickMain.bind(this, index)).node()
      );
      const t = d3.select(element).select("text")._groups[0][0];
      const tBox = t.getBBox();
      d3.select(t)
        .attr("x", x + (width - tBox.width) / 2)
        .attr("y", y + (height - tBox.height) / 2 + this.computeVal(26));
    });
  }

  renderPath() {
    this.svg.append("g");
    const g = this.svg.append("g");
    const mlCircle = d3.selectAll(".g-middle").selectAll(".m-l-circle")._groups[0][
      this.currMainRectIndex
    ];
    const mrCircle = d3.selectAll(".g-middle").selectAll(".m-r-circle")._groups[0][
      this.currMainRectIndex
    ];
    const leftCircles = d3.selectAll(".g-left").selectAll("circle");
    leftCircles._groups[0].forEach(el => {
      const line = LineComponent.create("l-left", this.subColor).node();
      line.setAttribute(
        "d",
        c(
          {
            x: mlCircle.cx.baseVal.value,
            y: mlCircle.cy.baseVal.value
          },
          { x: el.cx.baseVal.value, y: el.cy.baseVal.value }
        )
      );
      g.insert(() => line);
    });
    const rightCircles = d3.selectAll(".g-right").selectAll("circle");
    rightCircles._groups[0].forEach(el => {
      const line = LineComponent.create("l-right", this.subColor).node();
      line.setAttribute(
        "d",
        c(
          {
            x: mrCircle.cx.baseVal.value,
            y: mrCircle.cy.baseVal.value
          },
          { x: el.cx.baseVal.value, y: el.cy.baseVal.value }
        )
      );
      g.insert(() => line);
    });
  }

  draw() {
    // setTimeout(() => {
    const n = this.svg.node();
    this.width = n.getBoundingClientRect().width;

    this.computeVal = val => computeVal(val, this.width, 690);

    this.drawHeader();
    this.drawMiddle();

    this.renderFont();
    setTimeout(() => {
      this.onClickMain(0);
    }, 200);
  }

  destroy() {
    this.svg.remove();
  }
}

export default Graph;
