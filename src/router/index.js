import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Related from "../views/Related.vue";
import Detail from "../views/Detail.vue";
import Search from "../views/Search.vue";
import Own from "../views/Own.vue";
import Helper from "../views/Helper.vue";

Vue.use(VueRouter);

const routes = [
  { path: "/", name: "Home", component: Home },
  { path: "/detail", name: "Detail", component: Detail },
  {
    path: "/related",
    name: "Related",
    component: Related
  },
  {
    path: "/search",
    name: "Search",
    component: Search
  },
  {
    path: "/own",
    name: "own",
    component: Own
  },
  {
    path: "/helper",
    name: "helper",
    component: Helper
  }
];

console.log(process.env.BASE_URL);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.path === "/") {
    Com.setWebviewTitle("", { title: "    ", bgColor: "2f569e" }, true);
  }
  next();
});

export default router;
