import Vue from "vue";

import { List, Icon, Col, Row, Search, Popup, Overlay } from "vant";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "vant/lib/index.css";
import "./utils/app_common_bridge";
// import { getBridge } from "./utils";
import TalkingData from "./utils/TalkingData";

Vue.prototype.isThemeDark = false;
Vue.config.productionTip = false;

Vue.prototype.redirectNative = function(url) {
  // const bridge = getBridge();
  // if (IS_PROD && bridge) {
  //   const p =
  //     PUBLIC_PATH[PUBLIC_PATH.length - 1] === "/"
  //       ? PUBLIC_PATH.substring(0, PUBLIC_PATH.length - 1)
  //       : "";
  //   url = location.origin + p + url;
  //   Com.newWebViewOpenPage("", { pageUrl: url, weburl: url });
  // } else {
  //   this.$router.push(url);
  // }
  this.$router.push(url);
};

Vue.prototype.onEventWithLabel = function(key, value) {
  if (value === null || value === undefined) {
    value = key;
    key = "行情-产业链图谱";
  }

  TalkingData && TalkingData.onEventWithLabel(key, value);
};

Vue.use(Overlay);
Vue.use(List);
Vue.use(Icon);
Vue.use(Col);
Vue.use(Row);
Vue.use(Search);
Vue.use(Popup);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

console.log("20200521 21:59");
