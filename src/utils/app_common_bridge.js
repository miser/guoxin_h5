/**
 * @desc和app交互的共用的协议
 */
// import Zepto from '__ROOTPATH__/H5_common/asset/common_tool/zepto.js';
// require('__ROOTPATH__/H5_common/asset/common/js/finance-white-list.js');
// var $ = Zepto;
window.$js = window.$js || {}; //全局对象，所有提供给native调用的js方法都放在改对象里
("use strict");
var a = function() {},
  b = a.prototype;

/**
 * 调用native的方法
 * @param module    native的模块名(事先约定好)
 * @param method    native的方法名
 * @param params    传给native的参数
 * @param arguments[3]    callback  非必须参数
 * @param arguments[4]    callback params  非必须参数，用于在非webview环境测试
 * 注意：请先在$js对象下定义好callback(arguments[3])，然后再调用onCallNative方法
 */
function onCallNative(module, method, params) {
  var PROCOTOL = "guosen";
  //console.log(param);
  //console.log(callback);
  var l = "";
  var isMobile = Com.clientType();
  var delIframe = function(id) {
    var tmp = document.getElementById(id);
    if (tmp && tmp.remove) {
      tmp.remove();
    } else if (tmp) {
      document.body.removeChild(tmp);
    }
  };
  if (arguments.length == 3) {
    l = PROCOTOL + "://" + module + "/" + method + "/?params=" + JSON.stringify(params);
  } else if (arguments.length > 3) {
    l = PROCOTOL + "://" + module + "/" + method + "/?params=" + JSON.stringify(params);
    l = arguments[3] == "" ? l : l + "&callback=" + arguments[3];
  }

  if (isMobile == "android") {
    window.prompt(l);
  } else {
    var iframeid = module + method;
    var iframe = document.createElement("iframe");
    iframe.id = iframeid;
    iframe.src = l;
    iframe.style.display = "none";
    document.body.appendChild(iframe);
    setTimeout(function() {
      delIframe(iframeid);
    }, 100);
  }

  if (!(isMobile == "ios" || isMobile == "android")) {
    arguments[4] = arguments[4] || "{}";
    onCallJs('{"action":"' + arguments[3] + '","param":' + arguments[4] + "}");
  }
}
window.onCallNative = onCallNative;
/**
 * native调H5方法的统一入口
 * @param nativeParam  json字符串，eg:'{"action":"XXXXX","param":{key:value......}}'
 * @param arguments[1]  参数nativeParam是否encode处理
 */
function onCallJs(nativeParam) {
  var encode_flag = arguments[1];
  if (!nativeParam || nativeParam == null || nativeParam == "undefined" || nativeParam == "") {
    return;
  }
  //校验参数数据
  if (encode_flag && encode_flag == "1") {
    nativeParam = decodeURIComponent(nativeParam);
  }
  nativeParam = nativeParam.replace(/\r\n|\\r\\n|\\r|\\n/g, "<br/>");
  nativeParam = nativeParam.replace(/\s/gm, "&nbsp;");
  nativeParam = nativeParam.replace(/\\/gi, "");
  var _nativeParam = null;
  try {
    // alert(nativeParam.param);
    //nativeParam.action : 业务名称, nativeParam.param : 业务参数
    _nativeParam = typeof nativeParam == "object" ? nativeParam : JSON.parse(nativeParam);
    if (window.$js) {
      if (typeof $js[_nativeParam.action] == "function") {
        $js[_nativeParam.action](_nativeParam.param);
      }
    } else {
      window[_nativeParam.action](_nativeParam.param);
    }
  } catch (err) {
    //异常兼容处理
    if (!_nativeParam) {
      onCallJSCatchErr(nativeParam, err);
    } else {
      Com.showNativeAlert("", err.message, ["确定"]);
    }
  }
}
window.onCallJs = onCallJs;
function onCallJSCatchErr(nativeParam, err) {
  try {
    var _nativeParam = nativeParam.substring(1, nativeParam.length - 1);
    var _nativeParamArr = _nativeParam.split(",");
    var action = "";
    for (var i = 0; i < _nativeParamArr.length; i++) {
      if (_nativeParamArr[i].indexOf("action") >= 0) {
        action = _nativeParamArr[i].split(":")[1];
        action = action.substring(1, action.length - 1);
        break;
      }
    }
    if (window[action + "Error"] && typeof window[action + "Error"] == "function") {
      window[action + "Error"](nativeParam, _nativeParamArr);
    } else {
      Com.showNativeAlert("", action + "Error in not function", ["确定"], "");
    }
  } catch (err) {
    Com.showNativeAlert("", err.message, ["确定"], "");
  }
}
/**
 * 自定义ajax
 * @param obj(用来传递参数的对象)
 * @param url(接口地址)
 * @param data(json数据对象)
 * @param type(请求类型)
 * @param dataType(数据类型)
 * @param success(success)
 * @param isTradeLogin(是否需要交易登录，false或者true)
 * @param isNeedSign(是否需要签名，false或者true)
 * @param isNeedShareholder(是否需要股东账号(沪A)，false或者true，一般只有金天利的交易才需要)
 * @param isNeedLoading(是否需要加载等待loading效果，false或者true)
 * @param error(error)
 */
b.ajax = function(obj) {
  var obj = obj || {},
    that = this;
  if (typeof obj.success !== "function") {
    obj.success = function() {};
  }
  if (typeof obj.error !== "function") {
    obj.error = function() {};
  }
  if (!obj.dataType) {
    obj.dataType = "json";
  }
  if (!obj.type) {
    obj.type = "post";
  }
  if (typeof obj.async === "undefined") {
    obj.async = true;
  }
  if (!obj.isTradeLogin) {
    obj.isTradeLogin = false;
  }
  //if (!obj.isNeedSign) {
  obj.isNeedSign = true;
  //}
  if (!obj.isNeedLoading) {
    obj.isNeedLoading = false;
  }
  if (obj.data == null || obj.data == undefined) {
    obj.data = {};
  }
  //新增密码登录校验方式，1交易密码登录，2指纹人脸登录(默认是交易密码登录)
  if (!obj.supportBioLogin) {
    obj.supportBioLogin = "1";
  }

  obj.random = "_" + (Math.random() + "").split(".")[1];
  obj.data = Com.publicAppStorageArg(obj.data); //补齐缓存公共参数
  //定义js方法，供native调用，返回公共参数
  $js["setCommonLoginData" + obj.random] = function(param) {
    if (obj.isTradeLogin == true) {
      //定义js方法，供native调用，返回交易参数
      $js["setTradeLoginData" + obj.random] = function(tradeLoginData) {
        $.extend(param, tradeLoginData);
        _ajax(param);
      };
      //如果需要交易登录，从native获取交易参数

      /*onCallNative('DataService', 'getTradeLoginData', {}, 'setTradeLoginData' + obj.random, '{"fundId":"110000051505","randomKey":"inputtype=C&inputid=110002345509&custorgid=1100&orgid=1100&tradenode=9501&ext1=000&userinfo=~~~1100&authid=DD6646B32DCAD2869A02CA71040238657BE6A2D7AE388ED835E498F5B84ADE36","orgId":"1100","customId":"110000051505","authCode":"482399"}');*/
      //onCallNative('DataService', 'getTradeLoginData', {}, 'setTradeLoginData' + obj.random, '{"fundId":"110002323001","randomKey":"inputtype=C&inputid=110002345509&custorgid=1100&orgid=1100&tradenode=9501&ext1=0000&userinfo=~~~1100&authid=DD6646B32DCAD2869A02CA71040238650F987D415169609D844756AFA77F5625","orgId":"1100","customId":"110002323001","authCode":"482399"}');
      /*onCallNative('DataService','getTradeLoginData',{},'setTradeLoginData'+obj.random,'{"fundId":"110002542305","randomKey":"inputtype=C&inputid=110002542305&custorgid=1100&orgid=1100&tradenode=9501&ext1=001&userinfo=~~~1100&authid=DD6646B32DCAD2869A02CA710402386549E18318F560BCCA830B8DF29807A63C","orgId":"1100","customId":"110002542305","authCode":"482399"}');*/
      /*onCallNative('DataService','getTradeLoginData',{},'setTradeLoginData'+obj.random,'{"fundId":"110000013806","randomKey":"inputtype=C&inputid=110002345509&custorgid=1100&orgid=1100&tradenode=9501&ext1=000&userinfo=~~~1100&authid=DD6646B32DCAD2869A02CA7104023865ACF99D7B7A34A53B0C13A949BDAE519B","orgId":"1100","customId":"110002542305","authCode":"482399"}');*/
      /*onCallNative('DataService', 'getTradeLoginData', {"supportBioLogin":obj.supportBioLogin}, 'setTradeLoginData' + obj.random, '{"fundId":"110002345509","randomKey":"inputtype=C&inputid=110002345509&custorgid=1100&orgid=1100&tradenode=9501&ext1=0000&userinfo=~~~1100&authid=DD6646B32DCAD2869A02CA71040238654E29F37645B37C012F28C9A574E7B1A4","orgId":"1100","customId":"110002345509","authCode":"482399"}');*/
      onCallNative(
        "DataService",
        "getTradeLoginData",
        { supportBioLogin: obj.supportBioLogin },
        "setTradeLoginData" + obj.random,
        '{"fundId":"110000013806","randomKey":"inputtype=C&inputid=110000013806&custorgid=1100&orgid=1100&tradenode=9501&ext1=0000&userinfo=~~~1100&authid=F449541B5D8AE0ED3DAF9F14862A165E4E29F37645B37C01A80BFFF2CD97956A","orgId":"1100","customId":"110000013806","authCode":"482399"}'
      );

      //onCallNative('DataService', 'getTradeLoginData', {}, 'setTradeLoginData' + obj.random, '{"fundId":"110002345509","randomKey":"inputtype=C&inputid=110002345509&custorgid=1100&orgid=1100&tradenode=9501&ext1=0000&userinfo=~~~1100&authid=DD6646B32DCAD2869A02CA7104023865A686BED252BF552C2F629597349EC63C","orgId":"1100","customId":"110002345509","authCode":"804795"}');

      //测试用：在非webview环境下，模拟native调用js方法
      //onCallJs('{"action":"setTradeLoginData","param":{"fundId":"110002345509","randomKey":"Fundid:109866009:110002345509","orgId":"xxxx"}}');
    } else {
      _ajax(param);
    }
  };
  //从native获取公共参数

  onCallNative(
    "DataService",
    "getCommonLoginData",
    {},
    "setCommonLoginData" + obj.random,
    '{"session":"18072f6d84f6624709e9f071cf1b304db4e79275acff2be4af110691","packgeId":"H_101841","userCode":"1903955966170112","authCode":"482399","softName":"goldsun_android","sysVer":"5.4.6","hwID":"5F7439B5-E518-4133-9B54-A0DA7E41C8B2","mode":"debug","fundId":"110000013806","mobile":"18616782513","password":"123456asd","orgId":"1100","chkPkgId":false}'
  );

  function _ajax(param, md5) {
    //如果传了modifyCommonData，则表示要将公共参数里面的值对应着改成modifyCommonData中的值
    if (typeof obj.modifyCommonData !== "undefined") {
      for (var p in param) {
        if (obj.modifyCommonData[p]) {
          param[p] = obj.modifyCommonData[p];
        }
      }
    }

    /*参数筛选和转换*/
    var paramKeyJson = {
      nativeKey: [
        "netAddr",
        "softName",
        "sysVer",
        "hwID",
        "connStyle",
        "deviceVers",
        "session",
        "packgeId",
        "mip",
        "mac",
        "imsi" /*公共参数部分*/,
        "fundId",
        "randomKey",
        "orgId",
        "customId",
        "userCode",
        "authCode" /*交易参数部分*/
      ],
      backendKey: [
        "netAddr",
        "softName",
        "sysVer",
        "hwID",
        "connStyle",
        "deviceVers",
        "session",
        "pkg",
        "mip",
        "mac",
        "imsi" /*公共参数部分*/,
        "fundid",
        "randomKey",
        "orgId",
        "custid",
        "userCode",
        "authCode" /*交易参数部分*/
      ]
    };
    for (var i = 0, len = paramKeyJson.nativeKey.length; i < len; i++) {
      if (paramKeyJson.nativeKey[i] in param) {
        obj.data[paramKeyJson.backendKey[i]] = param[paramKeyJson.nativeKey[i]];
      }
    }
    //是否需要股东账号(沪A)，false或者true，一般只有金天利的交易才需要
    if (obj.isNeedShareholder == true) {
      for (var i = 0, len = param.shareholderList.length; i < len; i++) {
        if (
          param.shareholderList[i].shareholderMarketId == "1" &&
          param.shareholderList[i].shareholderSecuseq == "0"
        ) {
          obj.data.secuid = param.shareholderList[i].shareholderId;
        }
      }
    }
    param["formatType"] = "json"; //默认json格式
    param["reqDate"] = Com.getTodayDate(); //请求发送日期
    param["reqTime"] = new Date().getTime(); //请求发送时间

    if ("userCode" in param) {
      obj.data["userCode"] = param["userCode"];
    }

    var ajaxSubmit = function(param_sign) {
      if (param_sign) {
        obj.data = param_sign;
      }
      if (obj.isNeedLoading) Com.nativeLoading("true");
      $.ajax({
        url: obj.url,
        data: obj.data,
        dataType: obj.dataType,
        type: obj.type,
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        async: obj.async,
        success: function(respone) {
          //Com.postLog(obj.url,respone);
          if (obj.isNeedLoading) Com.nativeLoading("false");
          //code等于11-session过期
          if (respone != null && respone.result[0].code == 11) {
            Com.ajaxLoaded();
            Com.nativeToast("网络请求失败(" + respone.result[0].code + "),请重试");
            //如果公共参数的session过期，弹出提示，并调用getNewSession(native不在回调H5的方法，修改于20170227)

            /*$js['setNewSession'+obj.random]= function(param){
                         if (param.result == 1) {
                         var isMobile = Com.clientType();
                         if(isMobile == 'ios' || isMobile == 'android'){
                         $js['setCommonLoginData'+obj.random](param);
                         }
                         }else{
                         console.log(param.errorCode+':'+param.errorMessage);
                         }
                         };*/
            onCallNative(
              "DataService",
              "getNewSession",
              {},
              "setNewSession" + obj.random,
              '{"result":"1","session":"4aaf04fba6db4148b294098c684df558","packgeId":"xxxx","userCode":"xxxxx","authCode":"xxxxx"}'
            );
            obj.success(respone);
            return false;
          } else if (
            respone != null &&
            (respone.result[0].code == 18 || respone.result[0].code == 19)
          ) {
            Com.ajaxLoaded();
            //18-pkg过期失效，19-pkg无效
            Com.nativeToast("网络请求失败(" + respone.result[0].code + "),请重试");
            //定义js方法，供native调用，返回公共参数
            /*$js['setValidPkg'+obj.random]= function(param){
                         if (param.result == 1) {
                         var isMobile = Com.clientType();
                         if(isMobile == 'ios' || isMobile == 'android'){
                         $js['setCommonLoginData'+obj.random](param);
                         }
                         }else{
                         console.log(param.errorCode+':'+param.errorMessage);
                         }
                         };*/
            onCallNative(
              "DataService",
              "getValidPkg",
              {},
              "setValidPkg" + obj.random,
              '{"result":"1","session":"4aaf04fba6db4148b294098c684df558","packgeId":"xxxx","userCode":"xxxxx","authCode":"xxxxx"}'
            );
            obj.success(respone);
            return false;
          } else if (respone != null && respone.result[0].code == "22") {
            Com.ajaxLoaded();
            //账号被踢出(账号在其他设备登录，调用native的方法到手机号登录界面重新开始)
            //Com.showNativeAlert('',respone.result[0].msg+'返回的code是'+respone.result[0].code,['确定'],goLoginpage);
            Com.accountKicked(respone.result[0].msg);
            return false;
          } else {
            /*else if(respone!=null && respone.result[0].code == '0x0300001'){
                     //交易超时(后台返回的错误信息是:交易帐户自动退出;此时调用native的方法进行相关处理)
                     }*/
            obj.success(respone);
            //console.log(respone.result[0].code + ':' + respone.result[0].msg);
          }
        },
        error: function(a, b, c) {
          if (obj.isNeedLoading) Com.nativeLoading("false");
          if (obj.error && typeof obj.error === "function") {
            obj.error(a, b, c);
          } else {
            //console.log('请检查您的网络状况');
          }
        }
      });
    };

    //如果接口需要签名，将签名入参
    if (obj.isNeedSign) {
      getSecretSign(obj.data, ajaxSubmit, md5);
    } else {
      ajaxSubmit();
    }
  }
};
//add by swl
/*
 * 如果中台使用微服务架构，那么参数必须是json格式而非默认的formdata方式，因此需要改造通用ajax方法
 * */
b.ajaxJSON = function(obj) {
  console.log("run ajax json mode..." + JSON.stringify(obj));
  var obj = obj || {},
    that = this;
  if (typeof obj.success !== "function") {
    obj.success = function() {};
  }
  if (typeof obj.error !== "function") {
    obj.error = function() {};
  }
  if (!obj.dataType) {
    obj.dataType = "json";
  }
  if (!obj.type) {
    obj.type = "post";
  }
  if (typeof obj.async === "undefined") {
    obj.async = true;
  }
  if (!obj.isTradeLogin) {
    obj.isTradeLogin = false;
  }
  if (!obj.isNeedSign) {
    obj.isNeedSign = false;
  }
  if (!obj.isNeedLoading) {
    obj.isNeedLoading = false;
  }
  if (obj.data == null || obj.data == undefined) {
    obj.data = {};
  }
  obj.random = "_" + (Math.random() + "").split(".")[1];
  //obj.data = Com.publicAppStorageArg(obj.data);//补齐缓存公共参数
  //定义js方法，供native调用，返回公共参数
  $js["setCommonLoginData" + obj.random] = function(param) {
    if (obj.isTradeLogin == true) {
      //定义js方法，供native调用，返回交易参数
      $js["setTradeLoginData" + obj.random] = function(tradeLoginData) {
        $.extend(param, tradeLoginData);
        _ajaxJSON(param);
      };
      //如果需要交易登录，从native获取交易参数

      /*onCallNative('DataService', 'getTradeLoginData', {}, 'setTradeLoginData' + obj.random, '{"fundId":"110000051505","randomKey":"inputtype=C&inputid=110002345509&custorgid=1100&orgid=1100&tradenode=9501&ext1=000&userinfo=~~~1100&authid=DD6646B32DCAD2869A02CA71040238657BE6A2D7AE388ED835E498F5B84ADE36","orgId":"1100","customId":"110000051505","authCode":"482399"}');*/
      onCallNative(
        "DataService",
        "getTradeLoginData",
        {},
        "setTradeLoginData" + obj.random,
        '{"fundId":"110002345509","randomKey":"inputtype=C&inputid=110002345509&custorgid=1100&orgid=1100&tradenode=9501&ext1=0000&userinfo=~~~1100&authid=DD6646B32DCAD2869A02CA71040238650A484D773A7BA02389C96FF3C40576EE","orgId":"1100","customId":"110002345509","authcode":"482399"}'
      );
      /*onCallNative('DataService','getTradeLoginData',{},'setTradeLoginData'+obj.random,'{"fundId":"110002542305","randomKey":"inputtype=C&inputid=110002542305&custorgid=1100&orgid=1100&tradenode=9501&ext1=001&userinfo=~~~1100&authid=DD6646B32DCAD2869A02CA710402386549E18318F560BCCA830B8DF29807A63C","orgId":"1100","customId":"110002542305","authCode":"482399"}');*/
      /*onCallNative('DataService','getTradeLoginData',{},'setTradeLoginData'+obj.random,'{"fundId":"110000013806","randomKey":"inputtype=C&inputid=110002345509&custorgid=1100&orgid=1100&tradenode=9501&ext1=0000&userinfo=~~~1100&authid=DD6646B32DCAD2869A02CA71040238653AF8678B221D7A7D9F6B9F72B2732C67","orgId":"1100","customId":"110002542305","authCode":"482399"}');*/

      //测试用：在非webview环境下，模拟native调用js方法
      //onCallJs('{"action":"setTradeLoginData","param":{"fundId":"110002345509","randomKey":"Fundid:109866009:110002345509","orgId":"xxxx"}}');
    } else {
      _ajaxJSON(param);
    }
  };
  //从native获取公共参数

  onCallNative(
    "DataService",
    "getCommonLoginData",
    {},
    "setCommonLoginData" + obj.random,
    '{"session":"186aca1a2d839997d3f5c73b7193ce2f5fa22a60646289fa74cd8815","packgeId":"H_112781","userCode":"1715754744005632","authCode":"311572","softName":"goldsun_android","sysVer":"4.6.0","hwID":"5F7439B5-E518-4133-9B54-A0DA7E41C8B2","mode":"debug","fundId":"110002345509","mobile":"13482635522","password":"bamboo0502","orgId":"1100"}'
  );

  function _ajaxJSON(param) {
    //如果传了modifyCommonData，则表示要将公共参数里面的值对应着改成modifyCommonData中的值
    if (typeof obj.modifyCommonData !== "undefined") {
      for (var p in param) {
        if (obj.modifyCommonData[p]) {
          param[p] = obj.modifyCommonData[p];
        }
      }
    }

    /*参数筛选和转换*/
    var paramKeyJson = {
      nativeKey: [
        "netAddr",
        "softName",
        "sysVer",
        "hwID",
        "connStyle",
        "deviceVers",
        "session",
        "packgeId" /*公共参数部分*/,
        "fundId",
        "randomKey",
        "orgId",
        "customId",
        "userCode",
        "authCode" /*交易参数部分*/
      ],
      backendKey: [
        "netAddr",
        "softName",
        "sysVer",
        "hwID",
        "connStyle",
        "deviceVers",
        "session",
        "pkg" /*公共参数部分*/,
        "fundid",
        "randomKey",
        "orgId",
        "custid",
        "userCode",
        "authCode" /*交易参数部分*/
      ]
    };
    for (var i = 0, len = paramKeyJson.nativeKey.length; i < len; i++) {
      if (paramKeyJson.nativeKey[i] in param) {
        obj.data[paramKeyJson.backendKey[i]] = param[paramKeyJson.nativeKey[i]];
      }
    }
    //是否需要股东账号(沪A)，false或者true，一般只有金天利的交易才需要
    if (obj.isNeedShareholder == true) {
      for (var j = 0, len = param.shareholderList.length; j < len; j++) {
        if (param.shareholderList[j].shareholderMarketId == "1") {
          //上海市场
          obj.data.secuid = param.shareholderList[j].shareholderId;
          if (param.shareholderList[j].shareholderSecuseq == "0") {
            //上海主股东
            obj.data.secuid = param.shareholderList[j].shareholderId;
            break;
          }
        }

        /*if (param.shareholderList[j].shareholderMarketId == '1' && param.shareholderList[j].shareholderSecuseq == '0') {
                 obj.data.secuid = param.shareholderList[j].shareholderId;
                 }*/
      }
    }
    param["formatType"] = "json"; //默认json格式
    param["reqDate"] = Com.getTodayDate(); //请求发送日期
    param["reqTime"] = new Date().getTime(); //请求发送时间

    if ("userCode" in param) {
      obj.data["userCode"] = param["userCode"];
    }

    var ajaxJSONSubmit = function(param_sign) {
      if (param_sign) {
        obj.data = param_sign;
      }
      if (obj.isNeedLoading) Com.nativeLoading("true");
      //obj.data.randomKey = encodeURIComponent(obj.data.randomKey);
      $.ajax({
        url: obj.url,
        data: JSON.stringify(obj.data),
        dataType: obj.dataType,
        type: obj.type,
        contentType: "application/json;charset=utf-8",
        async: obj.async,
        success: function(respone) {
          //Com.postLog(obj.url,respone);
          if (obj.isNeedLoading) Com.nativeLoading("false");
          //code等于11-session过期
          if (respone != null && respone.result[0].code == 11) {
            Com.ajaxLoaded();
            Com.nativeToast("网络请求失败(" + respone.result[0].code + "),请重试");
            //如果公共参数的session过期，弹出提示，并调用getNewSession(native不在回调H5的方法，修改于20170227)

            /*$js['setNewSession'+obj.random]= function(param){
                         if (param.result == 1) {
                         var isMobile = Com.clientType();
                         if(isMobile == 'ios' || isMobile == 'android'){
                         $js['setCommonLoginData'+obj.random](param);
                         }
                         }else{
                         console.log(param.errorCode+':'+param.errorMessage);
                         }
                         };*/
            onCallNative(
              "DataService",
              "getNewSession",
              {},
              "setNewSession" + obj.random,
              '{"result":"1","session":"4aaf04fba6db4148b294098c684df558","packgeId":"xxxx","userCode":"xxxxx","authCode":"xxxxx"}'
            );
            obj.success(respone);
            return false;
          } else if (
            respone != null &&
            (respone.result[0].code == 18 || respone.result[0].code == 19)
          ) {
            Com.ajaxLoaded();
            //18-pkg过期失效，19-pkg无效
            Com.nativeToast("网络请求失败(" + respone.result[0].code + "),请重试");
            //定义js方法，供native调用，返回公共参数
            /*$js['setValidPkg'+obj.random]= function(param){
                         if (param.result == 1) {
                         var isMobile = Com.clientType();
                         if(isMobile == 'ios' || isMobile == 'android'){
                         $js['setCommonLoginData'+obj.random](param);
                         }
                         }else{
                         console.log(param.errorCode+':'+param.errorMessage);
                         }
                         };*/
            onCallNative(
              "DataService",
              "getValidPkg",
              {},
              "setValidPkg" + obj.random,
              '{"result":"1","session":"4aaf04fba6db4148b294098c684df558","packgeId":"xxxx","userCode":"xxxxx","authCode":"xxxxx"}'
            );
            obj.success(respone);
            return false;
          } else if (respone != null && respone.result[0].code == "22") {
            Com.ajaxLoaded();
            //账号被踢出(账号在其他设备登录，调用native的方法到手机号登录界面重新开始)
            //Com.showNativeAlert('',respone.result[0].msg+'返回的code是'+respone.result[0].code,['确定'],goLoginpage);
            Com.accountKicked(respone.result[0].msg);
            return false;
          } else {
            /*else if(respone!=null && respone.result[0].code == '0x0300001'){
                     //交易超时(后台返回的错误信息是:交易帐户自动退出;此时调用native的方法进行相关处理)
                     }*/
            obj.success(respone);
            console.log(respone.result[0].code + ":" + respone.result[0].msg);
          }
        },
        error: function(a, b, c) {
          if (obj.isNeedLoading) Com.nativeLoading("false");
          if (obj.error && typeof obj.error === "function") {
            obj.error(a, b, c);
          } else {
            console.log("请检查您的网络状况");
          }
        }
      });
    };

    //如果接口需要签名，将签名入参
    if (obj.isNeedSign) {
      getSecretSign(obj.data, ajaxJSONSubmit);
    } else {
      ajaxJSONSubmit();
    }
  }
};
/**
 * 账号被踢调用native的方法回到手机号登录界面
 * @param showMsg 账号被踢，后台给过来的错误提示信息
 * @param callback
 */
b.accountKicked = function(showMsg, callback) {
  var argumentsLength = arguments.length;
  var datajson = {};
  var random = "_" + (Math.random() + "").split(".")[1];
  var callbackName = "goLoginPage" + random;
  if (showMsg && showMsg != "") {
    datajson["showMsg"] = showMsg;
  }
  $js["goLoginPage" + random] = function(p) {
    if (argumentsLength > 1 && typeof callback === "function") {
      callback(p);
    }
  };
  onCallNative("DataService", "sessionTimeOut", datajson, callbackName);
};
/**
 * 获取客户端是ios还是Android
 */
b.clientType = function() {
  var u = navigator.userAgent;
  var app_type;
  //return false;
  if (u.indexOf("Android") > -1) {
    app_type = "android"; //android
  } else if (!!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
    app_type = "ios"; //ios
  }
  return app_type;
};

/**
 * 关闭ajaxloading
 */
b.ajaxLoaded = function() {
  Com.nativeLoading(false);
};

/**
 * ajaxloading用于ajax提交时的loading效果(css在common.css中)
 *
 */
b.ajaxLoading = function() {
  /*var imgurl = __uri('../../common/images/loading.gif');
     var loadingObj = $('<div class="loadingbox"><div class="loadingmain"><div class="loadingmask"></div><div class="loadingpic"><img src='+imgurl+' width="18" height="18"></div></div></div>');
     $('body').append(loadingObj);*/
  Com.nativeLoading(true);
};

/**
 * 判断登录状态
 * @param callback
 * @param param
 * @param loginType 1交易密码登录  2指纹登录
 */
b.isTradelogin = function(callback, param, loginType) {
  //默认判断是否做交易密码登录
  var datajson = param || {};
  if (typeof loginType !== "undefined" && loginType != "") {
    datajson.loginType = loginType;
  } else {
    //如果没有该参数，则询问是否交易密码登录
    datajson.loginType = "1";
  }
  onCallNative("DataService", "isTradeLogin", datajson, callback, "");

  //isTradeLogined:1登录；isTradeLogined:0未登录
  //onCallJs('{"action":"'+callback+'","param":{"isTradeLogined":1}}');
};

/**
 * 弹出native登录界面
 * @param callback
 * @param param
 * @param loginType 1密码登录 2支持指纹/人脸登录)
 */
b.callNativeLogin = function(callback, param, loginType) {
  //默认弹出交易密码登录界面
  var datajson = param || {};
  if (typeof loginType !== "undefined" && loginType != "") {
    datajson.supportBioLogin = loginType;
  } else {
    //如果没有该参数，则询问是否交易密码登录
    datajson.supportBioLogin = "1";
  }
  onCallNative("DataService", "doTradeLoginData", datajson, callback, "");

  //result:1登录成功；result!=1未登录||登录失败
  //onCallJs('{"action":"'+callback+'","param":{"result":"1","fundId":"xxxxx","orgId":"xxxx","randomKey":"xxxxx"}}');
};

/**
 * 新开webview打开页面(param里面多传一个weburl，用于本机浏览器调试)
 * @param callback
 * @param param
 * @param openflag 如果入参该参数则直接将该参数传给客户端标识游客是否可以查看
 * {pageUrl:"页面地址",touristAvl:1游客可用 0游客不可用,"weburl":"浏览器调试时传入的地址"}
 * touristAvl不传或者传空，客户端默认不允许游客访问
 */
b.newWebViewOpenPage = function(callback, param, openflag) {
  var datajson = param || {};
  var isMobile = Com.clientType();
  var allowstatus = "1"; //allowstatus==1允许游客访问  allowstatus=='0'不允许访问
  if (openflag) {
    allowstatus = openflag;
  } else {
    if (window.WHITE_LIST_JSON) {
      var FINANCE_ROOT = window.WHITE_LIST_JSON.FINANCE_ROOT; //理财目录
      var FINANCE_WHITELIST = window.WHITE_LIST_JSON.FINANCE_WHITE_LIST; //理财具体的白名单页面
      for (var i = 0; i < FINANCE_ROOT.length; i++) {
        if (datajson.pageUrl.indexOf(FINANCE_ROOT[i]) != -1) {
          //理财目录
          allowstatus = "0"; //理财目录默认游客都不能访问
          for (var j = 0; j < FINANCE_WHITELIST.length; j++) {
            if (datajson.pageUrl.indexOf(FINANCE_WHITELIST[j]) != -1) {
              //白名单列表可以访问
              allowstatus = "1";
              break;
            }
          }
        }
      }
    }
  }
  datajson.touristAvl = allowstatus;
  if (isMobile == "ios" || isMobile == "android") {
    onCallNative("WebContainer", "transitionWeb", datajson, callback);
  } else {
    if (param.weburl && param.weburl != "") {
      window.location.href = param.weburl;
    }
  }
};
/**
 * 设置webview页面title（单行title）
 * @param callback
 * @param param
 * {"title":"长安汽车-新闻","bgColor ":"#272736"}
 * 背景颜色，黑色：#272736，蓝色：#589dfc
 */
b.setWebviewTitle = function(callback, param, isCustColor) {
  var datajson = param || {};
  if (!isCustColor) {
    datajson = {
      title: datajson.title ? datajson.title : ""
    };
  }
  onCallNative("WebContainer", "titleItem", datajson, callback);
};
/**
 * 设置webview页面title（两行title）
 * @param callback
 * @param param
 * {"title":"长安汽车-新闻","bgColor ":"#272736"}
 * 背景颜色，黑色：#272736，蓝色：#589dfc
 */
b.setWebviewStockTitle = function(callback, param) {
  var datajson = param || {};
  onCallNative("WebContainer", "setStockTitle", datajson, callback);
};
/**
 * sessionStorage会话级存储
 * @param keys
 * @param values
 * @param expireDay
 */
b.setSessionValue = function(keys, values, expireDay) {
  var self = this;
  if (window.sessionStorage) {
    if (!values) {
      window.sessionStorage.setItem(keys, "");
    } else {
      window.sessionStorage.setItem(keys, encodeURIComponent(values)); //ios的Safari上,无痕模式会报错
    }
  } else {
    self.setCookie(keys, values, expireDay);
  }
};
/**
 * 设置cookie(仅当不支持本地存储时会触发该方法
 * @key
 * @value
 * expireTime(过期时间以天为单位)
 */
b.setCookie = function(sName, sValue, oExpires, sPath, sDomain, bSecure) {
  var sCookie = sName + "=" + encodeURIComponent(sValue);
  var d = new Date();
  if (oExpires) {
    d.setTime(d.getTime() + oExpires * 24 * 60 * 60 * 1000);
    sCookie += "; expires=" + oExpires.toUTCString();
  }
  if (sPath) {
    sCookie += "; path=" + sPath;
  }
  if (sDomain) {
    sCookie += "; domain=" + sDomain;
  }
  if (bSecure) {
    sCookie += "; secure";
  }
  document.cookie = sCookie;
};

/**
 * 获取cookie(仅当不支持本地存储时会触发该方法)
 *
 */
b.getCookie = function(key) {
  var c_start, c_end;
  if (document.cookie.length > 0) {
    c_start = document.cookie.indexOf(key + "=");
    if (c_start != -1) {
      c_start = c_start + key.length + 1;
      c_end = document.cookie.indexOf(";", c_start);
      if (c_end == -1) {
        c_end = document.cookie.length;
      }
      return decodeURIComponent(document.cookie.substring(c_start, c_end));
    }
  }
  return "";
};

/**
 * 获取根域名
 */
b.getOrigin = function() {
  var host;
  if (typeof window.location.origin === "undefined") {
    host = window.location.protocol + "//" + window.location.hostname;
  } else {
    host = window.location.origin;
  }
  return host;
};
/**
 * 获取url参数、解析客户端公共参数
 * @param name   //获取name的值
 * @param appStr //已有指定字符串
 * @returns {string}
 */
b.getQueryStringByName = function(name, appStr) {
  var result;
  if (appStr && appStr != "") {
    result = appStr.match(new RegExp("[?&]" + name + "=([^&]+)", "i"));
  } else {
    result = decodeURI(location.search).match(new RegExp("[?&]" + name + "=([^&]+)", "i"));
  }
  if (result == null || result.length < 1) {
    return "";
  }
  return result[1];
};

b.getTodayDate = function() {
  var odate = new Date();
  var year = odate.getFullYear();
  var month = odate.getMonth() + 1;
  var date = odate.getDate();
  var strfmt = function(num) {
    if (num < 10) {
      return "0" + num;
    }
    return num;
  };
  return year + "" + strfmt(month) + "" + strfmt(date);
};

/**
 * 判断是否支持本地存储
 * @returns {boolean}
 */
b.localStorageSupported = function() {
  try {
    localStorage.setItem("testlocal", "testlocal");
    localStorage.removeItem("testlocal");
    return true;
  } catch (e) {
    return false;
  }
};
/**
 * 本地存储值(如果不支持本地存储,会自动存cookie)
 * @key
 * @value
 * @expireDay(当不支持本地存储时,该参数当做cookie的过期时间)
 *
 */
b.setLocalValue = function(keys, values, expireDay) {
  var self = this;
  var isLocalSupported = Com.localStorageSupported();
  if (isLocalSupported) {
    if (!values) {
      window.localStorage.setItem(keys, "");
    } else {
      window.localStorage.setItem(keys, encodeURIComponent(values)); //ios的Safari上,无痕模式会报错
    }
  } else {
    self.setCookie(keys, values, expireDay);
  }
};

/**
 * 获取本地存储值(如果不支持本地存储,会自动获取cookie值)
 * @key
 *
 */
b.getLocalValue = function(keys) {
  var valueres,
    self = this;
  var isLocalSupported = Com.localStorageSupported();
  if (isLocalSupported) {
    valueres = decodeURIComponent(window.localStorage.getItem(keys));
  } else {
    valueres = self.getCookie(keys);
  }
  return valueres && valueres != "null" ? valueres : "";
};

/**
 * 删除本地存储的某个值(如果不支持本地存储,会自动删除cookie值)
 *
 */
b.removeLocalValue = function(keys) {
  var self = this;
  var isLocalSupported = Com.localStorageSupported();
  if (isLocalSupported) {
    window.localStorage.removeItem(keys);
  } else {
    self.setCookie(keys, "", -1);
  }
};
b.getWebTradeToken = function(callback) {
  var datajson = {};
  var random = "_" + (Math.random() + "").split(".")[1];
  var callbackName = "setWebTradeToken" + random;
  $js["setWebTradeToken" + random] = function(p) {
    var c = typeof callback;
    if (typeof callback === "function") {
      callback(p);
    }
  };
  onCallNative(
    "DataService",
    "getWebTradeToken",
    {},
    callbackName,
    '{"tradeToken":"xxxxxx","result":"xxxxxx","serverid":"xxxxxx","extprop":"xxxxxx","orgid":"xxxxxx","custid":"******"}'
  );
};
b.getToken = function(callback) {
  var datajson = {};
  var random = "_" + (Math.random() + "").split(".")[1];
  var callbackName = "setUserToken" + random;
  $js["setUserToken" + random] = function(p) {
    var c = typeof callback;
    if (typeof callback === "function") {
      callback(p);
    }
  };
  onCallNative(
    "DataService",
    "getUserToken",
    {},
    callbackName,
    '{"token":"3253117840753664","result":"******","message":""}'
  );
};
b.getSessionValue = function(keys) {
  var valueres,
    self = this;
  if (window.sessionStorage) {
    valueres = decodeURIComponent(window.sessionStorage.getItem(keys));
  } else {
    valueres = self.getCookie(keys);
  }
  return valueres;
};
b.isNumber = function(n) {
  var reg = /^\d+(\.\d+)?$/;
  return reg.test(n);
};
b.base64Decode = function(d) {
  var base64DecodeChars = [
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    62,
    -1,
    -1,
    -1,
    63,
    52,
    53,
    54,
    55,
    56,
    57,
    58,
    59,
    60,
    61,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    -1,
    -1,
    -1,
    -1,
    -1,
    -1,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    36,
    37,
    38,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51,
    -1,
    -1,
    -1,
    -1,
    -1
  ];
  var a, b, e, c, f;
  c = d.length;
  e = 0;
  for (f = ""; e < c; ) {
    do a = base64DecodeChars[d.charCodeAt(e++) & 255];
    while (e < c && -1 == a);
    if (-1 == a) break;
    do b = base64DecodeChars[d.charCodeAt(e++) & 255];
    while (e < c && -1 == b);
    if (-1 == b) break;
    f += String.fromCharCode((a << 2) | ((b & 48) >> 4));
    do {
      a = d.charCodeAt(e++) & 255;
      if (61 == a) return f;
      a = base64DecodeChars[a];
    } while (e < c && -1 == a);
    if (-1 == a) break;
    f += String.fromCharCode(((b & 15) << 4) | ((a & 60) >> 2));
    do {
      b = d.charCodeAt(e++) & 255;
      if (61 == b) return f;
      b = base64DecodeChars[b];
    } while (e < c && -1 == b);
    if (-1 == b) break;
    f += String.fromCharCode(((a & 3) << 6) | b);
  }
  return f;
};
b.bse64Encode = function(d) {
  var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  var a, b, e, c, f, g;
  e = d.length;
  b = 0;
  for (a = ""; b < e; ) {
    c = d.charCodeAt(b++) & 255;
    if (b == e) {
      a += base64EncodeChars.charAt(c >> 2);
      a += base64EncodeChars.charAt((c & 3) << 4);
      a += "==";
      break;
    }
    f = d.charCodeAt(b++);
    if (b == e) {
      a += base64EncodeChars.charAt(c >> 2);
      a += base64EncodeChars.charAt(((c & 3) << 4) | ((f & 240) >> 4));
      a += base64EncodeChars.charAt((f & 15) << 2);
      a += "=";
      break;
    }
    g = d.charCodeAt(b++);
    a += base64EncodeChars.charAt(c >> 2);
    a += base64EncodeChars.charAt(((c & 3) << 4) | ((f & 240) >> 4));
    a += base64EncodeChars.charAt(((f & 15) << 2) | ((g & 192) >> 6));
    a += base64EncodeChars.charAt(g & 63);
  }
  return a;
};
b.localhostType = function() {
  var hostType = "1"; //优先生产环境
  var host_name = location.hostname;
  if (host_name == "localhost") {
    //开启了H5缓存，当前地址是localhost
    //10000-10999生产
    //12000-12999测试
    //11000-11999开发
    var port = location.port;
    if (Number(port) >= 11000 && Number(port) <= 11999) {
      hostType = "dev";
    } else if (Number(port) >= 12000 && Number(port) <= 12999) {
      hostType = "test";
    } else {
      hostType = "1";
    }
  } else {
    var istestIpAddr = Com.isTestIp(host_name); //true测试环境或开发环境 false生产环境
    if (istestIpAddr) {
      if (host_name == "61.142.2.99") {
        hostType = "dev";
      } else {
        hostType = "test";
      }
    } else {
      hostType = "1";
    }
  }
  return hostType;
};
b.isLocalhost = function() {
  return window.location.host.indexOf("localhost") != -1;
};
/**
 * 进行通过app缓存加公共参数
 * @param param
 * @returns {*}
 */
b.publicAppStorageArg = function(param) {
  var gs_proxy_params = {};
  param = param ? $.extend({}, param) : {}; //深拷贝防止多次调用出问题
  if (Com.isLocalhost()) {
    if (param.gs_proxy_params) {
      gs_proxy_params = $.extend(param.gs_proxy_params, {
        gs_req_type: "data"
        //"gs_req_host": "jty"
      });
    } else {
      gs_proxy_params = {
        gs_req_type: "data"
        //"gs_req_host": "jty"
      };
    }

    param.gs_proxy_params = Com.bse64Encode(JSON.stringify(gs_proxy_params));

    //console.log(Com.base64Decode(param.gs_proxy_params));
    return param;
  } else {
    return param;
  }
};
/**
 * H5调native弹框
 * @param msgtitle 弹框标题，非必须
 * @param msg      弹框内容
 * @param alertbtn 弹框的按钮，例如["确定"]、["确定","取消"]
 * @param callback 弹框按钮的回调函数(会塞进来一个参数：{"alertViewId":"123456","clickedIndex":"1"})，clickedIndex表示按钮的索引
 * {"alertTitle":"弹框标题","alertMessage":"弹框内容","alertButtonsArray":["取消","确定"],"alertViewId":"数值累加，不用存到localstorage","webaLertMsg":"便于在浏览器调试"}
 */
b.showNativeAlert = function(msgtitle, msg, alertbtn, callback) {
  var datajson = {};
  var aletBoxId = Com.getSessionValue("alertboxid");
  if (!Com.isNumber(aletBoxId)) {
    aletBoxId = 0;
  } else {
    aletBoxId++;
  }
  Com.setSessionValue("alertboxid", aletBoxId);
  var random = "_" + (Math.random() + "").split(".")[1];
  var callbackName = "alertHandle" + random;
  $js["alertHandle" + random] = function(p) {
    if (callback && typeof callback === "function") {
      callback(p);
    }
  };
  datajson["alertTitle"] = msgtitle;
  datajson["alertMessage"] = msg;
  datajson["alertViewId"] = aletBoxId;
  datajson["alertButtonsArray"] = alertbtn;
  datajson["webaLertMsg"] = msg;
  var isMobile = Com.clientType();
  if (isMobile == "ios" || isMobile == "android") {
    onCallNative("WebContainer", "callNativeAlertView", datajson, callbackName);
  } else {
    alert(msg);
  }
};

/**
 * 判断是否登录，如果已登录则执行callback，如果未登录则先弹出native的登录，native登录成功之后执行callback，否则什么都不执行
 * @param callback 已登录或登录成功之后回调
 * @param param callback参数
 * @param cancelLogin 登录失败执行
 * @param loginTypeUI 1交易密码登录 2指纹登录
 */
b.actionLogin = function(callback, param, cancelLogin, loginTypeUI) {
  $js.checkTradeLogin = function(data) {
    if (typeof data === "string") {
      data = JSON.parse(data);
    }
    if (data.isTradeLogined == "1") {
      //已登录
      if (typeof callback === "function") {
        callback(param);
      }
    } else {
      //未登录，让native起登录
      $js.loginCallback = function(data) {
        if (data.result == "1") {
          //登录成功
          if (typeof callback === "function") {
            callback(param);
          }
        } else {
          //登陆失败
          if (cancelLogin && typeof cancelLogin === "function") {
            cancelLogin(data);
          }
          return false;
        }
      };
      //未登录
      Com.callNativeLogin("loginCallback", {}, loginTypeUI);
    }
  };

  //第一步判断是否登录(loginTypeUI 0交易密码登录 1指纹登录)
  Com.isTradelogin("checkTradeLogin", {}, loginTypeUI);
};

/**
 * 获取交易登录参数(使用之前需要交易登录Com.actionLogin)
 * @param callback
 * @param loginType 校验类型 1交易密码交易  2指纹校验
 */
b.getLoginData = function(callback, loginType) {
  var srandom = new Date().getTime();
  var callback_name = "setLoginData" + srandom;
  var datajson = { supportBioLogin: "1" };
  if (typeof loginType != "undefined" && loginType == "2") {
    datajson.supportBioLogin = "2";
  }
  $js["setLoginData" + srandom] = function(tradeLoginData) {
    if (typeof callback === "function") {
      callback(tradeLoginData);
    }
  };
  //从native获取交易参数
  onCallNative("DataService", "getTradeLoginData", datajson, callback_name, "");
};

/**
 * 是否需要native在当前页面上监听返回事件(每个页面都需要调用此方法,不监听的情况下，webview采用默认返回上一个webview的方式)
 * @param isListener 1:需要监听 0:不需要监听
 * @param callback 非必须参数
 */
b.nativeGobackListener = function(isListener, callback) {
  var argumentsLength = arguments.length;
  if (argumentsLength == 0) {
    return;
  }
  var datajson = {};
  var random = "_" + (Math.random() + "").split(".")[1];
  var callbackName = "listenerCallback" + random;
  $js["listenerCallback" + random] = function(p) {
    if (argumentsLength > 1 && typeof callback === "function") {
      callback(p);
    }
  };
  isListener == 1 ? (datajson["flag"] = "1") : (datajson["flag"] = "0");

  onCallNative("WebContainer", "setReturnListener", datajson, callbackName);
};

/**
 * 调用native的方法，返回上一个web页面
 */
b.browserGoback = function() {
  onCallNative("WebContainer", "doGoBack", {}, "");
};

/**
 * 调用native的方法关闭当前的webview(实际上相当于返回上一个webview)
 */
b.nativeGoback = function() {
  onCallNative("WebContainer", "doFinish", {}, "");
};

/**
 * 调用native的loading
 * @param isShow true显示/false隐藏
 */
b.nativeLoading = function(isShow) {
  var datajson = {};
  datajson["isShow"] = isShow;
  onCallNative("WebContainer", "showProcess", datajson, "");
};

/**
 * 跳转到股票行情
 * @param stockCode 股票代码
 * @param stockName 股票名称
 * @param stockMarket 股票市场
 */
b.gotoStockInfo = function(stockCode, stockName, stockMarket) {
  var datajson = {};
  datajson["stockCode"] = stockCode;
  datajson["stockName"] = stockName;
  datajson["stockMarket"] = stockMarket;
  onCallNative("WebContainer", "transitionStockInfo", datajson, "");
};

/**
 * 调用native的toast
 * @param type 显示类型  type:1添加自选股toast类型,其他为默认样式
 * @param msg 显示信息
 */
b.nativeToast = function(msg, type) {
  var datajson = {};
  datajson["toastMessage"] = msg;
  if (type) {
    datajson["toastType"] = type;
  }
  onCallNative("WebContainer", "showToastMsg", datajson, "");
};

/**
 * 右上角自定义按钮
 * @param btntype  0隐藏按钮 1分享 2设置字体 3:自定义按钮名称（必传btnname） 4:搜索按钮（放大镜图标）
 * @param btnname 当btntype == 3时，该参数必传
 * @param callback 点击回调
 * @param searchType 当btntype==4时需要传该参数，表示需要native搜索什么类型产品， 0：查全部 1：查基金 2:查国信产品
 * @param stkType 当btntype==4时需要传该参数，表示需要native搜索什么类型产品，0:全部 1:现金增利 2:资管 3:金天利 4:分级 5:金8    6: 信鑫宝 7:信鑫宝2   8:OTC
 */
b.rightTopBtn = function(btntype, btnname, callback, searchType, stkType) {
  var datajson = {};
  var _searchType = "";
  var _stkType = "";
  datajson["showType"] = btntype;
  if (btnname && btnname != "") {
    datajson["rightButtonName"] = btnname;
  }
  if (searchType && searchType != "") {
    _searchType = searchType;
  }
  if (stkType && stkType != "") {
    _stkType = stkType;
  }
  datajson["searchType"] = _searchType;
  datajson["stkType"] = _stkType;
  var random = "_" + (Math.random() + "").split(".")[1];
  var callbackName = "rightTopBtnHandle" + random;
  $js["rightTopBtnHandle" + random] = function(p) {
    if (callback && typeof callback === "function") {
      callback(p);
    }
  };
  onCallNative("WebContainer", "rightButtonItem", datajson, callbackName);
};
/**
 * 返回刷新，当该页面的webview再次呈现的时候native会调用callback(解决返回刷新的问题)
 * @param isrefresh true:再次呈现的时候需要调callback false:不需要
 * @param callback 非必须参数
 */
b.gobackRefreshPage = function(isrefresh, callback) {
  var argumentsLength = arguments.length;
  if (argumentsLength == 0) {
    return;
  }
  var datajson = {};
  var random = "_" + (Math.random() + "").split(".")[1];
  var callbackName = "refreshMethod" + random;
  $js["refreshMethod" + random] = function(p) {
    if (argumentsLength > 1 && typeof callback === "function") {
      callback(p);
    }
  };
  isrefresh == true ? (datajson["need"] = true) : (datajson["need"] = false);

  onCallNative("WebContainer", "needRefreshPage", datajson, callbackName);
};

/**
 * 跳转到native界面
 * @param type fun:native界面|web:web页面
 * @param nativeUrl xgsjh(跳转到native修改手机号)
 * @param param 打开native界面时传给native的参数
 */
b.openNativePage = function(nativeUrl, type, param) {
  var datajson = {};
  if (type && type != "") {
    datajson["handleType"] = type;
  } else {
    datajson["handleType"] = "fun";
  }
  if (typeof param !== "undefined") {
    datajson["handleParam"] = param;
  }
  datajson["handleUrl"] = nativeUrl;
  onCallNative("WebContainer", "transitionComNative", datajson, "");
};

/**
 * 获取公共参数
 * @param callback
 */
b.getCommonLoginData = function(callback) {
  var datajson = {};
  var random = "_" + (Math.random() + "").split(".")[1];
  var callbackName = "setData" + random;
  $js["setData" + random] = function(p) {
    if (typeof callback === "function") {
      callback(p);
    }
  };
  if (IS_PROD) {
    onCallNative(
      "DataService",
      "getCommonLoginData",
      datajson,
      callbackName
      // '{"session":"1481c3f1f7bdbee6856bb6bcc41e1399d0d5490f279db2d629aeaca2","packgeId":"H_132319","userCode":"1903955966170112","authCode":"482399","softName":"goldsun_android","sysVer":"1","hwID":"5F7439B5-E518-4133-9B54-A0DA7E41C8B2","deviceVers":"9|28"}'
    );
  } else {
    callback({
      session: "1481c3f1f7bdbee6856bb6bcc41e1399d0d5490f279db2d629aeaca2",
      packgeId: "H_132319",
      userCode: "1903955966170112",
      authCode: "482399",
      softName: "goldsun_android",
      sysVer: "1",
      hwID: "5F7439B5-E518-4133-9B54-A0DA7E41C8B2",
      deviceVers: "9|28"
    });
  }
};

b.getSelectedStock = function(callback) {
  $js["setSelectedStock"] = function(p) {
    if (typeof callback === "function") {
      callback(p);
    }
  };
  if (IS_PROD) {
    onCallNative("DataService", "getSelectedStock", {}, "setSelectedStock");
  } else {
    // onCallNative(
    //   "DataService",
    //   "getSelectedStock",
    //   {},
    //   "setSelectedStock",
    //   '{"selectedStock":["399001.1","000001.2","000987.1","600585.2","600585.2","002736.1","002738.1","002726.1"]}'
    // );
    callback({
      selectedStock: [
        "399001.1",
        "000001.2",
        "000987.1",
        "600585.2",
        "600585.2",
        "002736.1",
        "002738.1",
        "000001.SH",
        "002726.1"
      ]
    });
  }
};

/**
 * 判断当前IP是不是测试环境或者开发环境(有些功能需要区分是不是生产环境，例如风测的跳转)
 * @param ipAddr 传过来的ip地址
 */
b.isTestIp = function(ipAddr) {
  var testIpArry = [
    "172.24.140.61", //老的测试环境内网IP
    "58.61.28.213", //老的测试环境外网IP
    "61.142.2.99", //开发环境IP
    "172.24.150.100", //石波测试环境内网地址
    "61.142.2.100", //石波测试环境外网地址http
    "58.253.94.4" //石波测试环境外网地址https
  ];
  if (typeof ipAddr === "undefined" || ipAddr == "") {
    //生产环境优先
    return false;
  }
  for (var i = 0; i < testIpArry.length; i++) {
    if (testIpArry[i] == ipAddr) {
      return true;
    }
  }
  return false;
};
b.getRiskPagehost = function() {
  var riskUrlObj = {
    devUrl: Com.getOrigin() /*开发环境*/,
    testUrl: "http://58.253.94.4:1443" /*测试环境*/,
    productUrl: "https://ecsvc.guosen.com.cn:1443" /*生产环境*/
  };
  var riskUrl = riskUrlObj.productUrl;
  var host_name = location.hostname;
  if (host_name == "localhost") {
    //开启了H5缓存，当前地址是localhost
    //10000-10999生产
    //12000-12999测试
    //11000-11999开发
    var port = location.port;
    if (Number(port) >= 11000 && Number(port) <= 11999) {
      riskUrl = riskUrlObj.devUrl;
    } else if (Number(port) >= 12000 && Number(port) <= 12999) {
      riskUrl = riskUrlObj.testUrl;
    } else {
      riskUrl = riskUrlObj.productUrl;
    }
  } else {
    var istestIpAddr = Com.isTestIp(host_name); //true测试环境或开发环境 false生产环境
    if (istestIpAddr) {
      if (host_name == "61.142.2.99") {
        riskUrl = riskUrlObj.devUrl;
      } else {
        riskUrl = riskUrlObj.testUrl;
      }
    } else {
      riskUrl = riskUrlObj.productUrl;
    }
  }
  return riskUrl;
};
/**
 * 获取账户信息（资金账户、姓名）
 * @param callback
 * @param param
 */
b.getAccountInfo = function(callback, param) {
  var datajson = param || {};
  onCallNative("DataService", "getCurrentAccountInfo", datajson, callback, "");
};
/**
 * 将资金帐号或其他账号12位、10位的保留前4位和后两位，9位的保留前三位和后两位；其他原样显示（与金太阳保持一致）
 * @param str
 * @returns {*}
 */
b.repalceStr = function(str) {
  //return str.slice(0, 4) + Array(str.slice(5, -2).length + 1).join('*') + str.slice(-2);
  if (typeof str !== "string" || str == "") {
    return "";
  } else if (str.length > 9) {
    return str.slice(0, 4) + Array(str.slice(5, -2).length + 2).join("*") + str.slice(-2);
  } else if (str.length == 9) {
    return str.slice(0, 3) + Array(str.slice(4, -2).length + 2).join("*") + str.slice(-2);
  } else {
    return str;
  }
};

/**
 * 将名字的倒数第二位用*代替(与金太阳保持一致)
 * @param str
 * @returns {*}
 */
b.replaceName = function(str) {
  //return  Array(str.slice(0, -1).length + 1).join('*')+str.slice(-1);
  if (typeof str !== "string" || str == "") {
    return "";
  } else if (str.length >= 2) {
    return str.slice(0, -2) + "*" + str.slice(-1);
  } else {
    return "*";
  }
};
/**
 * 进行版本比较
 * @param sysVersion 系统版本
 * @param comparieVersion 要进行对比的版本
 * @return true: 系统版本大于或等于比较版本  false: 系统版本小于对比版本
 */
b.comparieVersion = function(comparieVersion, sysVersion) {
  if (!sysVersion || !comparieVersion) return;
  var sysArr = String(sysVersion).split("."),
    comparieArr = String(comparieVersion).split(".");
  var len = Math.max(sysArr.length, comparieArr.length);

  for (var i = 0; i < len; i++) {
    if (!sysArr[i]) sysArr[i] = 0;
    if (!comparieArr[i]) comparieArr[i] = 0;
    if (Number(sysArr[i]) > Number(comparieArr[i])) {
      return true;
    } else if (Number(sysArr[i]) < Number(comparieArr[i])) {
      return false;
    }
  }
  return true;
};

/**
 * H5的签名算法，依赖md5.js且需要再common之前引入
 * @param param 业务参数
 * @param callback 取签名之后的回调函数
 * @returns {boolean}
 */
function getSecretSign(param, callback) {
  if (!param || !callback) {
    return;
  }
  var array = [];
  var sign = "";
  var cbrandow = "_" + (Math.random() + "").split(".")[1];
  //目前客户端两种平台下的key
  var mockSign = "";
  var secretSign = "";
  $js["getSecretKeyCallBack" + cbrandow] = function(secretkey) {
    var skey = secretkey.signKey;
    //param['softName'] = 'goldsun_android';/*现在在公共参数返回softName*/
    for (var i in param) {
      //仅当value的值不是null或者不是空字符串，才处理
      if (param[i] !== null && param[i] !== "") {
        array.push(i);
      }
    }
    array.sort();

    for (var j = 0; j < array.length; j++) {
      sign += array[j] + "" + param[array[j]];
    }
    mockSign = skey + "" + sign + "" + skey;
    if (typeof md5 !== "undefined") {
      console.log("md5");
      secretSign = md5.hex(mockSign).toLocaleUpperCase();
    } else if (typeof hex_md5 !== "undefined") {
      console.log("hex_md5");
      secretSign = hex_md5(mockSign).toLocaleUpperCase();
    } else {
      secretSign = mockSign.toLocaleUpperCase();
    }
    param["openapi_sign"] = secretSign;

    callback(param);
  };
  onCallNative(
    "DataService",
    "getSignKey",
    "",
    "getSecretKeyCallBack" + cbrandow,
    '{"signKey":"E3080A2B4F794002BEF5A57F3B06CF70"}'
  );
}
/**
 *
 * @param partName
 * @param methodName
 * @param callback
 * @param dataJson
 */
b.callNativeFun = function(partName, methodName, callback, dataJson) {
  var random = "_" + (Math.random() + "").split(".")[1];
  var callbackName = "setCommonValue" + random;
  $js[callbackName] = function(param) {
    if (typeof callback === "function") {
      callback(param);
    }
  };
  onCallNative(partName, methodName, dataJson, callbackName);
};
b.isChinese = function(str) {
  if (/[\u4e00-\u9fa5]/g.test(str)) {
    return true;
  } else {
    return false;
  }
};
String.prototype.replaceAll = function(reallyDo, replaceWith, ignoreCase) {
  if (!RegExp.prototype.isPrototypeOf(reallyDo)) {
    return this.replace(new RegExp(reallyDo, ignoreCase ? "gi" : "g"), replaceWith);
  } else {
    return this.replace(reallyDo, replaceWith);
  }
};
String.prototype.gblen = function() {
  var len = 0;
  for (var i = 0; i < this.length; i++) {
    if (this.charCodeAt(i) > 127 || this.charCodeAt(i) == 94) {
      len += 2;
    } else {
      len++;
    }
  }
  return len;
};
//兼容ios切WkWebview bug
var u = navigator.userAgent;
if (!!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
  var isPageHide = false;
  window.addEventListener("pageshow", function() {
    if (isPageHide) {
      window.location.reload();
    }
  });
  window.addEventListener("pagehide", function() {
    isPageHide = true;
  });
}
window.Com = new a();
export default Com;
