import axios from "axios";
import qs from "qs";

const instance = axios.create();

location.ancestorOrigins;

if (IS_PROD) {
  instance.defaults.baseURL = `${location.origin}/gsnews/industrychain`;
} else {
  instance.defaults.baseURL = "//guoxinindustrychain.aiyechen.com/gsnews/industrychain/";
}

instance.defaults.timeout = 15000;

// instance.interceptors.request.use(
//   function(config) {
//     // Do something before request is sent
//     console.log(config);
//     config.headers["Content-Type"] = "application/x-www-form-urlencoded";
//     // Content-Type: "application/json;charset=utf-8"
//     console.log(config.headers);
//     return config;
//   },
//   function(error) {
//     // Do something with request error
//     return Promise.reject(error);
//   }
// );

instance.interceptors.response.use(
  async response => {
    if (response.config.method.toUpperCase() === "OPTIONS") return response;

    const { data: res } = response;

    if (res.retCode === "000" && res.retMsg === "success") {
      return Promise.resolve(res.data);
    }
    // alert(JSON.stringify(res));
    return Promise.reject(res.retMsg);
  },
  err => Promise.reject(err)
);

export default {
  post(url, json) {
    const options = {
      method: "POST",
      headers: { "content-type": "application/x-www-form-urlencoded" },
      data: qs.stringify(json),
      url
    };
    return instance(options);
  },
  get(url, data) {
    return instance.get(url, {
      params: data
    });
  }
};
