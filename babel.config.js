module.exports = {
  presets: [
    ["@vue/cli-plugin-babel/preset"]
    // [
    //   "@vue/app",
    //   {
    //     polyfills: ["es.promise", "es.symbol"]
    //   }
    // ]
  ],
  plugins: [
    [
      "import",
      {
        libraryName: "vant",
        libraryDirectory: "es",
        style: true
      },
      "vant"
    ]
  ]
};
