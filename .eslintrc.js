module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "@vue/airbnb"],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    "no-bitwise": "off",
    "implicit-arrow-linebreak": "off",
    "function-paren-newline": "off",
    "space-before-function-paren": "off",
    "object-shorthand": "off",
    "prefer-destructuring": "off",
    "no-plusplus": ["error", { allowForLoopAfterthoughts: true }],
    "no-underscore-dangle": "off",
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    quotes: ["error", "double"],
    "arrow-parens": ["error", "as-needed"],
    "comma-dangle": ["error", "never"]
  }
};
